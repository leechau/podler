import typer
import pkg_resources
import podler.downloader as pld
from rich.pretty import pprint
from rich.console import Console
from rich.table import Table

FEEDS = 'feeds.yml'
CLIP = 'clips.yml'
HIST = 'history.yml'

app = typer.Typer(context_settings={"help_option_names": ["-h", "--help"]})


@app.command('adf')
def add_feed(name: str, url: str, extractor: str):
    '''Add a new feed'''
    pld.add_feed(name, url, extractor)

@app.command('lsf')
def list_feeds():
    '''List feed names in the feeds file'''
    feeds = pld.get_feed_list()

    table = Table(title="Podcast Feed List")
    table.add_column("Name", justify="right", style="cyan", no_wrap=True)
    table.add_column("Format", style="magenta")
    console = Console()
    for feed in feeds:
        table.add_row(feed['name'], feed['extractor'])
    console.print(table)


@app.command('cfg')
def configure(key: str = typer.Option(None, "--key", "-k"),
              val: str = typer.Option(None, "--val", "-v")):
    '''Configure podler'''
    pprint(pld.config(key, val))


@app.command('lsc')
def list_clips(feed_names: str):
    '''List new clips for the specific feed(s)'''
    pld.build_clip_list(feed_names)


@app.command('dlc')
def download_selected_clips(idx: str):
    '''Download a clip'''
    pld.download_clips(idx)


@app.command('syn')
def sync_configurations():
    '''Synchronize configurations with remote repository'''
    print(pkg_resources.require("podler")[0].version)


@app.command('ver')
def version():
    '''Print app version'''
    print(pkg_resources.require("podler")[0].version)


@app.callback()
def callback():
    """Clip downloader.

    Workflow:

    1. Add a / list existing feed(s): `adf` / `lsf`

    2. List *clips* (episodes) in a feed: `lsc`

    3. Download a clip: `dlc`
    """


def main():
    app()

if __name__ == "__main__":
    main()

