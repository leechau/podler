from strictyaml import Seq, Map, Str, Url, Int, Bool, Datetime, load, as_document
import feedparser
import os
import shutil
from pathlib import Path
import subprocess as sp
from yt_dlp import YoutubeDL

CONF_HOME = Path.home() / '.config/podler'

FEEDF = CONF_HOME / 'feeds.yml'
CLIPF = CONF_HOME / 'clips.yml'
HISTF = CONF_HOME / 'history.yml'
CONFF = CONF_HOME / 'config.yml'

DEFAULT_CONF = '''use_proxy: true
proxy: "http://localhost:2340"
'''

CONF_SCHEMA = Map({
    'use_proxy': Bool(),
    'proxy': Str()
})

FEED_SCHEMA = Seq(Map({
    'name': Str(),
    'url': Str(),
    'extractor': Str(),
}))

CLIP_SCHEMA = Seq(Map({
    'id': Str(),
    'idx': Int(),
    'title': Str(),
    'link': Url(),
    'feed': Str(),
    'date': Datetime(),
}))

HIST_SCHEMA = Seq(Map({'title': Str(), 'id': Str()}))


def extract_url(res, name: str):
    if name == 'youtube':
        return res.link
    elif name == 'voa':
        return res.links[0].href
    return None


def config(keyn: str = None, valn: str = None) -> dict:
    if not CONFF.exists():
        print(f'Create default configuration file: {CONFF}')
        with open(CONFF, 'w') as f:
            f.write(DEFAULT_CONF)
    with open(CONFF, 'r') as f:
        confs = load(f.read(), schema=CONF_SCHEMA).data
    if keyn is None:
        return confs
    if valn is None:
        return { keyn: confs[keyn] }
    else:
        confs[keyn] = valn
        with open(CONFF, 'w') as f:
            f.write(as_document(confs, schema=CONF_SCHEMA).as_yaml())
        return confs


def add_feed(name: str, url: str, extractor: str):
    pass


def get_feed_list() -> dict:
    if not FEEDF.exists():
        return {}
    with open(FEEDF, 'r') as f:
        feeds = load(f.read(), schema=FEED_SCHEMA).data
    return feeds


def build_clip_list(feed_names: str):
    names = feed_names.split(',')
    if config(keyn='use_proxy')['use_proxy']:
        os.environ['HTTPS_PROXY'] = config(keyn='proxy')['proxy']

    with open(FEEDF, 'r') as f:
        feeds = load(f.read(), schema=FEED_SCHEMA).data

    selected = []
    for feed in feeds:
        if not (feed['name'] in names):
            continue
        fi = feedparser.parse(feed['url'])
        fi.update(extractor=feed['extractor'])
        selected.append(fi)

    clips = []
    idx = 1
    for item in selected:
        for ent in item.entries:
            clip = {'id': ent.id,
                    'idx': idx,
                    'title': ent.title,
                    'link': extract_url(ent, item.extractor),
                    'feed': item.feed.title,
                    'date': ent.published }
            idx = idx + 1
            clips.append(clip)

    with open(CLIPF, 'w') as f:
        f.write(as_document(clips, schema=CLIP_SCHEMA).as_yaml())

    history = []
    if Path(HISTF).is_file():
        with open(HISTF, 'r') as f:
            history = load(f.read(), schema=HIST_SCHEMA).data
    downloaded = [item['id'] for item in history]

    clip_list = []
    for c in clips:
        dled = '*' if c['id'] in downloaded else ''
        clip = f'{c["idx"]}{dled}: {c["title"]}, {c["date"]}, {c["feed"]}'
        clip_list.append(clip)

    print('CLIPS LIST:\n')
    print('\n'.join(clip_list))


def download_clips(idx: str):
    if config(keyn='use_proxy')['use_proxy']:
        os.environ['HTTPS_PROXY'] = config(keyn='proxy')['proxy']

    ids = [int(i) for i in idx.split(',')]
    with open(CLIPF, 'r') as f:
        clips = load(f.read(), schema=CLIP_SCHEMA).data

    pendings = [clip for clip in clips if clip['idx'] in ids]

    # history = []
    # if Path(HISTF).is_file():
        # with open(HISTF, 'r') as f:
            # history = load(f.read(), schema=hist_schema).data
    # downloaded = [item['id'] for item in history]

    for c in pendings:
        url = c["link"]
        if url.endswith('mp3'):
            fn = c['title'].split(' ')[0]
            with requests.get(
                url,
                proxies={'http': config(keyn='proxy')['proxy'],
                         'https': config(keyn='proxy')['proxy']},
                stream=True) as r:
                with open(fn, "wb") as f:
                    shutil.copyfileobj(r.raw, f)
                print(f'Episode saved to {fn}')
        else:
            ydl_opts = {
                'proxy': config(keyn='proxy')['proxy'],
                'format': 'm4a/bestaudio/best',
                'postprocessors': [{
                    'key': 'FFmpegExtractAudio',
                    'preferredcodec': 'm4a',
                }]
            }
            with YoutubeDL(ydl_opts) as ydl:
                error_code = ydl.download([url])
        # if not (c['id'] in downloaded):
            # history.append({'title': c['title'], 'id': c['id']})
            # with open(HISTF, 'w') as f:
                # f.write(as_document(history, schema=hist_schema).as_yaml())
            # print(f'Clip #{c["idx"]} downloaded!\n')

