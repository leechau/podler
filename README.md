# Podler

My podcast downloader.

```
from urllib.request import urlretrieve
res_path, headers = urlretrieve('http://python.org/')
urlretrieve('http://python.org/', 'aa.html')
```

## Prerequisites

Install ffmpeg with `sudo apt install ffmpeg`.

## Data Structure Design

### RSS Feeds

* title: string
* URL: string
* extractor: how to extract url from feed

### Video/Audio Clips

* ID: clip ID
* index: saved in clips.yml, for selecting clip by user
* title: string
* link: string, URL of the clip
* feed: which feed the clip belongs to
* date: clip's publishing date

## Usage

### Feed Manipulation

* Add/delete/update a feed: edit feeds.yml manually

* List feeds: `podler lsf`

### Clip Manipulation

* List clips of one or more feed(s):  `podler lsc f2` or `podler lsc f1,f2`

* Download clips selected by indices in argument: `podler dl 3` or `podler dl 4,8,12`

## Get Feed URL

1. Open the chennel you want to subscribe in browser and get its URL, for example:
   `https://www.youtube.com/channel/UC8UCbiPrm2zN9nZHKdTevZA`. Now you get the channel
   ID: `UC8UCbiPrm2zN9nZHKdTevZA`
1. Append the ID to `https://www.youtube.com/feeds/videos.xml?channel_id=`.
   For example with above URL you get the full path of feed:
   `https://www.youtube.com/feeds/videos.xml?channel_id=UC8UCbiPrm2zN9nZHKdTevZA`

Ref: [How to Get an RSS Feed for a YouTube Channel](https://danielmiessler.com/blog/rss-feed-youtube-channel/)

## Deveopment

```
poetry run -C ~/Documents/podler python ~/Documents/podler/podler/app.py cfg -k use_proxy
```

## Roadmap

* Sync configuration and feed list;
* Support range (e.g.: 3,5-8) for index selection
* Plugin structure: youtube, voa, etc
* Operate on mobile devices
* Update clips automatically
